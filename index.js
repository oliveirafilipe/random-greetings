const csv = require('csvtojson');
const csvFilePath='ocupacoes.csv';

const awesomeProfessions = [];

const DISPLAY_EVERY = process.env.DISPLAY_EVERY_MS || 2000; //miliseconds
csv({
	delimiter: ';'
})
.fromFile(csvFilePath)
.then((professions)=>{
    for(const profession of professions) {
    	if(profession.DESCRICAO.toLowerCase().match(/dor|sor/))
    		awesomeProfessions.push(profession.DESCRICAO);
    }

    setInterval(doRandomGreeting, DISPLAY_EVERY);
});

function doRandomGreeting() {
	const randomIndex = getRandomArbitrary(0, awesomeProfessions.length);
	const profession = awesomeProfessions[randomIndex];
	const normalizedProfession = nomalize(profession);
	const greeting = getGreeting();
   	console.log(`${greeting} meu ${normalizedProfession}`);
}

function getRandomArbitrary(min, max) {
    return Math.ceil(Math.random() * (max - min) + min);
}

function nomalize(word){
	return word.toLowerCase()
    .split(' ')
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ')
	.replace(/\(.*\)/gm,'')
	.replace(/Sem Especifica(ca|ça|çã)o/gm, '');
}

function getGreeting() {
	const hour = new Date().getHours();
	if(hour >= 0 && hour < 6) return "Boa Madrugada"
	if(hour >= 6 && hour < 12) return "Bom Dia"
	if(hour >= 12 && hour < 18) return "Boa Tarde"
	if(hour >= 18) return "Boa Noite"

	return "Fala"
}
