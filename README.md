# random-greetings

Displays random brazilian greetings in a meme style, like:
```
Fala meu Gravador
Fala meu Programador De Materiais
Fala meu Criador De Ras
Fala meu Aferidor De Produtividade
Fala meu Enramador
Fala meu Preparador De Couros E Peles Curtidas
Fala meu Domador
Fala meu Recebedor De Apostas
Fala meu Controlador De Patio De Sucata
Fala meu Prensador De Frutas
```

## To run with Docker
`docker-compose up --build`

## To run locally
`npm start`