FROM node:10

WORKDIR /app

ADD package.json .
RUN npm install
ADD index.js .
ADD ocupacoes.csv .

CMD ["npm", "start"]